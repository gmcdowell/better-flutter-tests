# Change Log

All notable changes to the "better-tests" extension will be documented in this file.

## [0.3.2] - 2021-09-01

### Changes
- fixes minor syntax error

## [0.3.1] - 2021-08-17

### Changes
- Use flutter_test pkg in place of dart test pkg
- Update npm modules (audit fix)
- Fix test_file_creator bugs
- Update snippets to reflect flutter_test use

## [0.1.8] - 2021-02-01

- Initial release

## [0.2.0] - 2021-02-22
### Added
- StatusBarItem that shows the number of found tests for the file. 